
--CREATE DATABASE MyDatabase

BEGIN TRAN

USE MyDatabase

CREATE TABLE tblCustomer (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	FirstName VARCHAR(50) NULL,
	Surname VARCHAR(50) NULL,
	Gender INT NULL,
	Created DATETIME NULL,
	Active BIT NOT NULL DEFAULT 0
)

SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'tblCustomer'

INSERT INTO tblCustomer (FirstName, Surname, Gender, Created, Active) VALUES ('Anthony', 'Ward', 0, GETDATE(), 1)

SELECT * FROM tblCustomer

--ROLLBACK TRAN
COMMIT TRAN
