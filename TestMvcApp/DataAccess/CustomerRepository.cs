﻿using System.Collections.Generic;
using System.Linq;
using TestMvcApp.Models;

namespace TestMvcApp.DataAccess
{
    public class CustomerRepository
    {
        public List<CustomerModel> GetCustomers()
        {
            return null;
        }

        public List<CustomerModel> GetAll()
        {
            using (var context = new DataContext())
            {
                var data = context.Customers.ToList();
                return data;
            }
        }

        public void InsertCustomer(CustomerModel model)
        {
            using (var context = new DataContext())
            {
                context.Customers.Add(model);
                context.SaveChanges();
            }
        }

        public void UpdateCustomer(CustomerModel model)
        {
            if (model.Id <= 0)
            {
                InsertCustomer(model);
                return;
            }

            using (var context = new DataContext())
            {
                var entity = context.Customers.Single(x => x.Id == model.Id);
                entity.FirstName = model.FirstName;
                entity.Surname = model.Surname;
                entity.Gender = model.Gender;
                entity.Active = model.Active;
                context.SaveChanges();
            }
        }
    }
}