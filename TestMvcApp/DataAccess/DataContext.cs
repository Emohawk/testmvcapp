﻿using System.Data.Entity;
using TestMvcApp.Models;

namespace TestMvcApp.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext() : base("MyDatabaseConnection")
        {
        }

        public DbSet<CustomerModel> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerModel>().ToTable("tblCustomer");
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }
    }
}