﻿using System.Collections.Generic;
using System.Web.Mvc;
using TestMvcApp.DataAccess;
using TestMvcApp.Models;

namespace TestMvcApp.Controllers
{
    public class HomeController : Controller
    {
        private CustomerRepository customerRepository;

        public HomeController()
        {
            customerRepository = new CustomerRepository();
        }

        public ActionResult Index()
        {
            var customers = customerRepository.GetAll();
            return View(customers);
        }

        [HttpGet]
        public ActionResult GetCustomers()
        {
            // Not used
            return null;
        }

        [HttpGet]
        public ActionResult AddCustomer()
        {
            ViewBag.Genders = new List<SelectListItem>
            {
                new SelectListItem { Text = "Please Select..", Value = "-1", Selected = true },
                new SelectListItem { Text = "Male", Value = "0", Selected = false },
                new SelectListItem { Text = "Female", Value = "1", Selected = false }
            };
            return View();
        }

        [HttpPost]
        public ActionResult AddCustomer(CustomerModel model)
        {
            customerRepository.InsertCustomer(model);
            return RedirectToAction("Index");
        }
    }
}