﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestMvcApp.Models
{
    [Table("tblCustomer")]
    public class CustomerModel
    {
        public CustomerModel()
        {
            Created = DateTime.Now;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int Gender { get; set; }
        public DateTime Created { get; set; }
        public bool Active { get; set; }
    }
}